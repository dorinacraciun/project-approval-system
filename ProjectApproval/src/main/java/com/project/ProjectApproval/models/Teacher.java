package com.project.ProjectApproval.models;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
//@Table(name="teacher")
public class Teacher extends User {

    @OneToMany(mappedBy = "teacher")
    private List<Feedback> feedbacks = new ArrayList<Feedback>();

    public Teacher() {
    }

    public Teacher(String username, String password, String firstName, String lastName, String email) {
        super(username, password, firstName, lastName, email);
    }
}
