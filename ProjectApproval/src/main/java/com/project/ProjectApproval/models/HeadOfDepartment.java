package com.project.ProjectApproval.models;

import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
//@Table(name="head_of_department")
public class HeadOfDepartment extends User {

    @OneToOne(mappedBy = "headOfDept")
    private Department department;

    @OneToMany(mappedBy = "student")
    private List<Task> tasks = new ArrayList<Task>();
}
