package com.project.ProjectApproval.models;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name="department")
public class Department {
    @Id @GeneratedValue
    private Integer id;
    private String name;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(unique = true, name = "head_of_dept", referencedColumnName = "id")
    private HeadOfDepartment headOfDept;
}
