package com.project.ProjectApproval.models;

import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Data
@Entity
@Table(name="project")
public class Project {
    @Id
    @GeneratedValue
    private Integer id;
    private String title;
    private String repository_url;
    private Integer grade;

    @OneToMany(mappedBy = "project")
    private List<Task> tasks = new ArrayList<Task>();

    @OneToMany(mappedBy = "project")
    private List<Feedback> feedbacks = new ArrayList<Feedback>();

    @ManyToMany(mappedBy = "projects")
    private Set<Student> students = new HashSet<>();
}
