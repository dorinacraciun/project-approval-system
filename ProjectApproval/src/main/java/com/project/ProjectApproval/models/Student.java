package com.project.ProjectApproval.models;

import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Data
@Entity
//@Table(name="student")
public class Student extends User{

    @OneToMany(mappedBy = "student")
    private List<Task> tasks = new ArrayList<Task>();

    @ManyToMany(cascade = { CascadeType.ALL })
    @JoinTable(
            name = "student_project",
            joinColumns = { @JoinColumn(name = "student_id") },
            inverseJoinColumns = { @JoinColumn(name = "project_id") }
    )
    Set<Project> projects = new HashSet<>();
}
