package com.project.ProjectApproval.models;

import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Data
@Entity
@Table(name="role")
public class Role {
    @Id
    @GeneratedValue
    private Long id;
    private String name;

//    @ManyToMany(mappedBy = "roles")
//    private Set<User> user = new HashSet<>();

    @OneToMany(mappedBy = "role")
    private List<User> users = new ArrayList<User>();
}
