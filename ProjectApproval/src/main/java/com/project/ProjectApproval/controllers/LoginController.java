package com.project.ProjectApproval.controllers;

import com.project.ProjectApproval.models.User;
import com.project.ProjectApproval.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

//@RestController()
@Controller
public class LoginController {

//    @RequestMapping(value = "/login", method = RequestMethod.POST)
//    @ResponseBody
//    public String loginPost(@ModelAttribute("user") User user) {
//        System.out.println("ACUM E POST");
//        System.out.println(user);
//        return "Cevaaaa";
//    }

    @Autowired
    private UserService userService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @GetMapping("/")
    public String index(){
        System.out.println("A introdus doar / ,deci login");
        return "/login";
    }

//    @RequestMapping(value = "/login", method = RequestMethod.GET)
//    public String login(){
//        System.out.println("ACUM AVEM UN GET");
//

    @GetMapping("/login")
    public String login(HttpServletRequest request) throws IOException, ServletException {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        System.out.println("auth " + auth);
        System.out.println("roles " + auth.getAuthorities());
        if (!(auth instanceof AnonymousAuthenticationToken) && auth.getAuthorities().contains("ROLE_ADMIN")) {
            return "/home";
        }
        else if (!(auth instanceof AnonymousAuthenticationToken) && auth.getAuthorities().contains("ROLE_STUDENT")) {
            return "/student";
        }
        return "/login";
    }

    @GetMapping("/register")
    public String registerPage() {
        return "/register";
    }

    @PostMapping("/register")
    public String registerSubmit(
            @RequestParam("firstName") String firstName,
            @RequestParam("lastName") String lastName,
            @RequestParam("username") String username,
            @RequestParam("email") String email,
            @RequestParam("password") String password,
            RedirectAttributes redirectAttributes
    ) {
        if (userService.findByUsername(username) == null) {
            User newUser = new User(username, passwordEncoder.encode(password), firstName, lastName, email);
            userService.saveUser(newUser);

            redirectAttributes.addFlashAttribute("successMsg", "Registered successfully! Now you can sign in using your login and password!");

            return "redirect:/";
        } else {
            redirectAttributes.addFlashAttribute("errorMsg", "This username is already taken!");
            return "redirect:/register";
        }
    }

    @GetMapping("/home")
    @ResponseBody
    public String home(){
        System.out.println("redirecting to homepage...");
        return "redirect:/home";
    }

    @GetMapping("/student")
    @ResponseBody
    public String student(){
        System.out.println("student");
        return "/student.html";
    }

    @GetMapping("/test")
    public String test(){

        return "Alex";
    }
}
