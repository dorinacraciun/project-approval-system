package com.project.ProjectApproval.controllers;

import com.project.ProjectApproval.models.User;
import com.project.ProjectApproval.repositories.RoleRepository;
import com.project.ProjectApproval.repositories.UserRepository;
import com.project.ProjectApproval.services.UserService;
import lombok.var;
import org.hibernate.service.spi.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(path = "users")
public class UserController {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @RequestMapping("")
    String home(ModelMap modal) {
        modal.addAttribute("title", "CRUD Example");
        return "index";
    }

    @GetMapping()
    public String getAllUsers(Model model) {
        List<User> users = userService.findAll();
        model.addAttribute("users", users);
        System.out.println("lista de utilizatori" + users);
        return "users/index";
    }

    @GetMapping(path = "{id}")
    public String getUserById(Model model, @PathVariable("id") Long id) {
        if (id != null) {
            Optional<User> user = userService.findById(id);
            if (user == null) {
                System.out.println("User with id " + id + " not found");
            } else {
                model.addAttribute("user", user);
            }
        }
        return "user/show";
    }

    @GetMapping(value = "/new")
    public String create(Model model, @ModelAttribute User user) {
        model.addAttribute("user", user);
        return "user/create";
    }

    @PostMapping(value = "/create", headers = "Accept=application/json",produces= MediaType.APPLICATION_JSON_VALUE)
    public String create(@RequestBody User entity, BindingResult result, RedirectAttributes redirectAttributes) {
        User user = null;
        System.out.println("Acum facem create pentru un nou utilizator" + entity);
        try {
            entity.setPassword(passwordEncoder.encode(entity.getPassword()));
            user = userService.createUser(entity);
//            redirectAttributes.addFlashAttribute("success", MSG_SUCESS_INSERT);
        } catch (Exception e) {
//            redirectAttributes.addFlashAttribute("error", MSG_ERROR);
            e.printStackTrace();
        }
        return "redirect:/users/" + user.getId();
    }

    @GetMapping("/{id}/edit")
    public String update(Model model, @PathVariable("id") Long id) {
        try {
            if (id != null) {
                Optional<User> entity = userService.findById(id);
                model.addAttribute("user", entity);
            }
        } catch (Exception e) {
            throw new ServiceException(e.getMessage());
        }
        return "user/update";
    }

    @PutMapping
    public String update(@Valid @ModelAttribute User entity, BindingResult result, RedirectAttributes redirectAttributes) {
        User user = null;
        try {
            user = userService.updateUser(entity);
//            redirectAttributes.addFlashAttribute("success", MSG_SUCESS_UPDATE);
        } catch (Exception e) {
//            redirectAttributes.addFlashAttribute("error", MSG_ERROR);
            e.printStackTrace();
        }
        return "redirect:/users/" + user.getId();
    }
}

//    @DeleteMapping("/{id}")
//    public String delete(@PathVariable("id") Integer id, RedirectAttributes redirectAttributes) {
//        try {
//            if (id != null) {
//                Long idL = Long.valueOf(id);
//                var entity = userService.findById(Long.valueOf(id));
//                if(entity.isPresent()) {
//                    userService.deleteUser(entity.get());
////                redirectAttributes.addFlashAttribute("success", MSG_SUCESS_DELETE);
//                }
//            }
//        } catch (Exception e) {
////            redirectAttributes.addFlashAttribute("error", MSG_ERROR);
//            throw new ServiceException(e.getMessage());
//        }
//        return "redirect:/users/index";
//    }
//}
