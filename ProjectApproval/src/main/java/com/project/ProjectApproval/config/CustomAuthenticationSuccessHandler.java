package com.project.ProjectApproval.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.WebAttributes;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Set;

@Configuration
public class CustomAuthenticationSuccessHandler implements AuthenticationSuccessHandler {

//    @Override
//    public void onAuthenticationSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication) throws IOException, ServletException {
//
//        System.out.println("Intru pe aici");
//        Set<String> roles = AuthorityUtils.authorityListToSet(authentication.getAuthorities());
//        System.out.println(roles);
////        if (roles.contains("ROLE_ADMIN")) {
////            // redirect catre pagina de admin cu toate tool-urile
////            httpServletResponse.sendRedirect("/home");
////        } else {
////            //redirect catre pagina de student
////            httpServletResponse.sendRedirect("/student");
////        }
//    }

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
                                        Authentication authentication) throws IOException, ServletException {
        clearAuthenticationAttributes(request);

        Set<String> roles = AuthorityUtils.authorityListToSet(authentication.getAuthorities());
        System.out.println("rolul este" + roles);
        if (roles.contains("ROLE_ADMIN")) {
            // redirect catre pagina de admin cu toate tool-urile
            response.sendRedirect("/login");
        } else {
            //redirect catre pagina de student
            response.sendRedirect("/student");
        }
    }

    private void clearAuthenticationAttributes(final HttpServletRequest
                                                       request) {
        final HttpSession session = request.getSession(false);

        if (session == null) {
            return;
        }

        session.removeAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);


    }
}
