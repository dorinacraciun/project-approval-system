package com.project.ProjectApproval.config;

import com.project.ProjectApproval.CustomAuthenticationFailureHandler;
import com.project.ProjectApproval.RESTAuthenticationEntryPoint;
import com.project.ProjectApproval.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.HttpStatusEntryPoint;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.*;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;
import java.util.Set;

//.defaultSuccessUrl("/home", true)
//        .failureUrl("/login")
@Configuration
@EnableWebSecurity
@ComponentScan
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    DataSource dataSource;

    //Enable jdbc authentication
    @Autowired
    public void configAuthentication(AuthenticationManagerBuilder auth) throws Exception {
        auth.jdbcAuthentication().dataSource(dataSource);
    }

    private AuthenticationSuccessHandler authenticationSuccessHandler;
    @Autowired
    UserService userService;

    @Autowired
    private RESTAuthenticationEntryPoint authenticationEntryPoint;

    @Autowired
    public SecurityConfig(AuthenticationSuccessHandler authenticationSuccessHandler) {
        this.authenticationSuccessHandler = authenticationSuccessHandler;
    }


    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/", "/home", "/register", "/js/**", "/css/**","/resources/**","/static/**").permitAll()
                .anyRequest()//allow all urls
                .authenticated()//all URLs are allowed by any authenticated user, no role restrictions.
        .and()
                .formLogin()//enable form based authentication
                .loginPage("/login")//use a custom login URI
                .failureHandler(customAuthenticationFailureHandler())
                .successHandler(authenticationSuccessHandler)
                .permitAll(true)//login URI can be accessed by anyone
        .and()
                .logout()//default logout handling
                .permitAll()//allow all as it will be accessed when user is not logged in anymore
        .and()
                .csrf()
                .disable()
         .exceptionHandling()
                .authenticationEntryPoint(new HttpStatusEntryPoint(HttpStatus.UNAUTHORIZED));

    }


    @Override
    public void configure(AuthenticationManagerBuilder builder)
            throws Exception {
        String password = passwordEncoder().encode("123");
        System.out.println("parola este" + password);
        builder.inMemoryAuthentication()
                .withUser("joe")
                .password(password)
                .roles("ADMIN");

        builder.inMemoryAuthentication()
                .withUser("student")
                .password(password)
                .roles("STUDENT");
    }

    public AuthenticationFailureHandler customAuthenticationFailureHandler() {
        return new CustomAuthenticationFailureHandler();
    }

    @Bean
    @Override
    public UserDetailsService userDetailsService() {
        UserDetails user =
                User.withDefaultPasswordEncoder()
                        .username("user")
                        .password("password")
                        .roles("USER")
                        .build();
        UserDetails admin =
                User.withDefaultPasswordEncoder()
                        .username("admin")
                        .password("admin")
                        .roles("ADMIN")
                        .build();

//        com.project.ProjectApproval.models.User user1 = new com.project.ProjectApproval.models.User("user", "password", "", "", "");
//        com.project.ProjectApproval.models.User user2 = new com.project.ProjectApproval.models.User("admin", "admin", "", "", "");
//
//        userService.saveUser(user1);
//        userService.saveUser(user2);

        return new InMemoryUserDetailsManager(user, admin);
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}

