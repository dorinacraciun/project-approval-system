package com.project.ProjectApproval.repositories;

import com.project.ProjectApproval.models.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Long> {
}
