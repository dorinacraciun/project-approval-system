package com.project.ProjectApproval.repositories;

import com.project.ProjectApproval.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {
    List<User> findAll();
    List<User> findByLastName (String lastName);
    List<User> findByFirstName (String firstName);
    User findByEmail (String email);
    User findByUsername (String username);

//    void delete(Long id);
}
