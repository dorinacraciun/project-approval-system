package com.project.ProjectApproval;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

import com.project.ProjectApproval.models.User;

import java.io.Console;
import java.util.Arrays;

@SpringBootApplication
public class ProjectApprovalApplication  {

	public static void main(String[] args) {
		SpringApplication.run(ProjectApprovalApplication.class, args);
	}
}
