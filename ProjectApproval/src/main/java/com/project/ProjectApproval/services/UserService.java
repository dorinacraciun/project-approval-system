package com.project.ProjectApproval.services;

import com.project.ProjectApproval.models.User;
import com.project.ProjectApproval.repositories.RoleRepository;
import com.project.ProjectApproval.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
public class UserService implements UserDetailsService {
    @Autowired
    private final UserRepository userRepository;

    @Autowired
    private final RoleRepository roleRepository;

    @Autowired
    private final PasswordEncoder passwordEncoder;

    public UserService(UserRepository repository, RoleRepository roleRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = repository;
        this.roleRepository = roleRepository;
        this.passwordEncoder = passwordEncoder;
    }

    public List<User> findAll(){
        return userRepository.findAll();
    }

    public Optional<User> findById(Long id) {
        return userRepository.findById(id);
    }

    public List<User> findByLastName(String lastName) {
        List<User> user = userRepository.findByLastName(lastName);
        return user;
    }

    public List<User> findByFirstName(String FirstName) {
        List<User> user = userRepository.findByFirstName(FirstName);
        return user;
    }

    public User findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    public boolean isUserExist(User user) {
        return findByUsername(user.getUsername()) != null;
    }

    public User findByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    public User createUser(User user) {
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        return userRepository.save(user);
    }

    public User saveUser(User user) {
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        user.setRole(roleRepository.getOne(1L));
        userRepository.save(user);
        return user;
    }

    public User updateUser(User user){
        User userUpdated = saveUser(user);
        return userUpdated;
    }

    public void deleteUser(User user){
        userRepository.delete(user);
    }
//
//    public void deleteAllUsers(){
//        userRepository.deleteAll();
//    }



    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException("user not found");
        }
        return createSpringUser(user);
    }
    private org.springframework.security.core.userdetails.User createSpringUser(User user) {
        return new org.springframework.security.core.userdetails.User(
                user.getEmail(),
                user.getPassword(),
                Collections.singleton(createAuthority(user)));
    }
    //nu stiu daca functioneaza bine
    private GrantedAuthority createAuthority(User user) {
        return new SimpleGrantedAuthority(user.getRole().getName());
    }
}
