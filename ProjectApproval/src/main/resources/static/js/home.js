window.onload = function () {

    function validateNewUser(userData){
        return true;
    }

    function closeModal(modalElement) {
        modalElement.modal('hide');
    }

    function veziLucrarea(){
        window.location.href = "../html/detaliiLucrare.html"
    }


    function descarcaPDF(){
        alert("se descarca pdf - de implementat")
    }

    function trimitereMail(){
        success = function(result){
            console.log("email  trimis", result);
        };

        error = function(err){
            alert("Nu s-a putut trimite email-ul. Incearca din nou")
        };

        $.ajax({
            type: "POST",
            url: "http://localhost:8080/sendemail",
            data: null,
            success: success,
            error: error
        });

    }
    function addNewUser() {

        var username = document.querySelector("#username").value;
        var firstName = document.querySelector("#firstName").value;
        var lastName = document.querySelector("#lastName").value;
        var email = document.querySelector("#email").value;
        var password = document.querySelector("#password").value;

        var data = {
            "username" : username,
            "firstName": firstName,
            "lastName" : lastName,
            "email" : email,
            "password":password
        };

        console.log("data trimis este",data);
        // data.username = username;
        // data.firstName = firstName;
        // data.lastName = lastName;
        // data.email = email;
        // data.password = password;
        if(validateNewUser(data)){

            success = function(result){
                console.log("User creat cu succes ", result);

                var addNewUserModal =    $('#addNewUser');
                closeModal(addNewUserModal);
            };


            error = function(err){
                console.log("Eroarea este ", err);
                if(err.status >= 200 && err.status <=300 ){
                    alert("User creat cu success")
                    var addNewUserModal =    $('#addNewUser');
                    closeModal(addNewUserModal);

                }
                else{
                    alert("Formularul NU este valid")
                }
            };

            $.ajax({
                type: "POST",
                url: "http://localhost:8080/users/create",
                contentType:"application/json; charset=utf-8",
                data: JSON.stringify(data),
                success: success,
                error: error
            });

        }
        else{
            alert("naspa")
        }
    }

    function saveProfile() {
        var editMyProfileModal =    $('#editMyProfile');
        closeModal(editMyProfileModal);
    }

    var submitLoginBtn = document.querySelector(".save-user");
    var saveProfileBtn = document.querySelector(".save-profile");
    var veziLucrareaBtn = $(".vezi-lucrarea");
    var DescarcaPDFBtn = $(".descarca-pdf");
    var sendEmail = $(".trimitere-mail");


    submitLoginBtn.addEventListener("click",addNewUser);
    saveProfileBtn.addEventListener("click",saveProfile);

    // jquery ca sa adaugam pe toate butoanele fara un for
    $(veziLucrareaBtn).on('click',veziLucrarea);
    $(DescarcaPDFBtn).on('click',descarcaPDF);
    $(sendEmail).on('click',trimitereMail);

};
