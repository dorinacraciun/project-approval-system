# Project Approval System

In this Online Project Approval System project, we will focus mainly on automating 
the process of project submission. In the sense project topics will be submitted 
online along with doc and approval will be provided online by the head of the 
department along with suggestions if any.
Students can also update their project status and provide in for regarding the 
progress, which will be monitored by all relevant professors and head of departments. 

### [Issue boards](https://gitlab.com/dorinacraciun/project-approval-system/-/boards)

### [ER diagram](https://dbdiagram.io/d/5e178f986ef5ab67de30e559)

#### [Example](https://ketarn.herokuapp.com/)